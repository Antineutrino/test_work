# test_work
Тестовое задание

Начало работы
==
**linux**
Установить пакет python3-dev
    sudo apt install python3-dev

Установить MongoDB
    sudo apt install mongodb-server-core

Установка и запуск виртуального окружения, установка необходимых пакетов
    python3 -m venv .env
    source .env/bin/activate
    pip install -r requirements.txt

Создать директорию и запустить экземпляр MongoDB отдельным процессом в виртуальном окружении:
    mkdir data/db
    mongod

Запуск проекта: python main.py



Модели данных
==
Сотрудник: id, name, lastname, patronymic, sex, age, coordinates. id для короткой идентификации и удобного
поиска




===========================================================================================================
Дальше в попытках разобраться всё пошло не по плану, и эта информация исключительно для тех, кому интересно
ковыряться в этой неудачной попытке выполнения задания. Все питоновские файлы кроме main.py служили для
тестирования "в уголке" различных возможностей.


Сотрудник:
{"id":"1", "name":"Artemiy", "lastname":"Lebedev", "patronymic":"Tatyanich", "sex":"m", "age":"32", "coordinates":[[time, lat, long], [time, lat, long]]}

Первоначальное добавление без координат, в дальнейшем координаты добавляются в словарь

Как-то так оно работает, в базу добавляется:

curl 127.0.0.1:8000/add -d '{"id": "1", "name": "Artemiy", "lastname": "Lebedev", "patronymic": "Tatyanich", "sex": "m", "age": "32", "coordinates": [["2018-10-16 14:58:56.039194", 55.154491, 61.370554]]}'


curl 127.0.0.1:8000/add -d '{"id": "1", "name": "Artemiy", "lastname": "Lebedev", "patronymic": "Tatyanich", "sex": "m", "age": "32"}'