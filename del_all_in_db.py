import motor.motor_asyncio
import asyncio
import pprint

client = motor.motor_asyncio.AsyncIOMotorClient()
client = motor.motor_asyncio.AsyncIOMotorClient('localhost', 27017)
db = client.test_database
collection = db.test_collection




async def do_delete_many():
    coll = db.test_collection
    n = await coll.count_documents({})
    print('%s documents before calling delete_many()' % n)
    result = await db.test_collection.delete_many({})
    print('%s documents after' % (await coll.count_documents({})))


async def do_delete_one(id):
    await db.test_collection.delete_one({"id": id})



loop = asyncio.get_event_loop()

loop.run_until_complete(do_delete_many())

#loop.run_until_complete(do_delete_one(2))

#loop.run_until_complete(do_count())