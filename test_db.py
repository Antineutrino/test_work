import motor.motor_asyncio
import asyncio
import pprint
from sanic import response #from sanic.response import json
import json
from bson.json_util import dumps

client = motor.motor_asyncio.AsyncIOMotorClient('localhost', 27017)
db = client.test_database
collection = db.test_collection

async def do_find_one():
    document = await db.test_collection.find_one({'key': 'value'})
    pprint.pprint(document)
    print(type(document))
    print(str(document))
    print(type(str(document)))
    print(type(document['_id']))
    doc = dumps(document)
    print(doc)
    print(type(doc))
    j = json.loads(doc)
    print(j)
    print(type(j))


loop = asyncio.get_event_loop()
loop.run_until_complete(do_find_one())