import motor.motor_asyncio
import asyncio
import pprint

client = motor.motor_asyncio.AsyncIOMotorClient()
client = motor.motor_asyncio.AsyncIOMotorClient('localhost', 27017)
db = client.test_database
collection = db.test_collection

async def do_insert():
    document = {'key2': 'value2'}
    result = await db.test_collection.insert_one(document)
    print('result %s' % repr(result.inserted_id))

async def do_find():
    cursor = db.test_collection.find({'key1':'value1'})
    for document in await cursor.to_list(length=100):
        pprint.pprint(document)

async def do_find2():
    cursor = db.test_collection.find({})   #db.test_collection.find({'key6':'value6'})
    for document in await cursor.to_list(length=100):
        pprint.pprint(document)

async def do_count():
    n = await db.test_collection.count_documents({})
    print('%s documents in collection' % n)
    #n = await db.test_collection.count_documents({'key6': 'value6'})
    #print('%s documents with value6' % n)

async def do_replace():
    coll = db.test_collection
    old_document = await coll.find_one({'key2': 'value2'})
    print('found document: %s' % pprint.pformat(old_document))
    _id = old_document['_id']
    result = await coll.replace_one({'_id': _id}, {'key': 'value'})
    print('replaced %s document' % result.modified_count)
    new_document = await coll.find_one({'_id': _id})
    print('document is now %s' % pprint.pformat(new_document))

async def do_update():
    coll = db.test_collection
    result = await coll.update_one({'key2': 'value2'}, {'$set': {'key': 'value3'}})
    print('updated %s document' % result.modified_count)
    new_document = await coll.find_one({'key': 'value3'})
    print('document is now %s' % pprint.pformat(new_document))



async def do_delete_many():
    coll = db.test_collection
    n = await coll.count_documents({})
    print('%s documents before calling delete_many()' % n)
    result = await db.test_collection.delete_many({'key': 'value3'})
    print('%s documents after' % (await coll.count_documents({})))



loop = asyncio.get_event_loop()
#loop.run_until_complete(do_insert())
#loop.run_until_complete(do_find())
loop.run_until_complete(do_find2())
loop.run_until_complete(do_count())
#loop.run_until_complete(do_replace())
#loop.run_until_complete(do_count())
#loop.run_until_complete(do_update())
#loop.run_until_complete(do_count())

#loop.run_until_complete(do_delete_many())
#loop.run_until_complete(do_count())