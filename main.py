from sanic import Sanic
from sanic import response # json, html
from sanic.response import text

import json
from bson.json_util import dumps

import motor.motor_asyncio
import asyncio


client = motor.motor_asyncio.AsyncIOMotorClient('localhost', 27017)
db = client.test_database
collection = db.test_collection

async def do_find_one(id):
    document = await db.test_collection.find_one({"id": id})
    doc = dumps(document)
    j = json.loads(doc)
    return j

async def do_insert(request):
    document = request
    result = await db.test_collection.insert_one(document)
    print('result %s' % repr(result.inserted_id)) #добавить return с id-ом добавленного

async def do_delete_one(id):
    await db.test_collection.delete_one({"id": id})

async def do_count():
    n = await db.test_collection.count_documents({})
    return n

app = Sanic()

@app.route('/')
async def test(request):
    s = {'hello': 'world'}
    return response.json(s)

@app.route('/add', methods=['POST'])
async def post_json(request):
    loop = asyncio.get_event_loop()
    s = request.json
    loop.run_until_complete(do_insert(s))
    return response.json({'status': 'people is added'})

@app.route('/del', methods=['POST'])
async def post_json(request):
    request = request.json
    i = request.get('id')
    loop = asyncio.get_event_loop()
    loop.run_until_complete(do_delete_one(i))
    return response.json({'status': 'people is deleted'})

@app.route('/get', methods=['POST'])
async def post_json(request):
    request = request.json
    i = request.get('id')
    loop = asyncio.get_event_loop()
    loop.run_until_complete(do_find_one(i))
    return text('POST request - {}'.format(request.json))





if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8000)