from datetime import datetime
from dateutil.parser import parse  #это работает
import json

now = datetime.now()
print('now: ', now)
print(type(now))
sn = str(now)
print('str now: ', sn)
print(type(sn))
dt = parse(sn)  #это работает, пригодится когда будет реализация выборки по времени
print('dt: ', dt)
print(type(dt))



#s = {"name":"Artemiy", "lastname":"Lebedev", "patronymic":"Tatyanich", "sex":"m", "age":"32",
#     "coordinates":[[sn, 55.154491, 61.370554]]}


s = {"id": 1, "name":"Artemiy", "lastname":"Lebedev", "patronymic":"Tatyanich", "sex":"m", "age":32}

sd = {"id": 2}
sj = json.dumps(sd) #строка

print(s)
print(type(s))

print(sj)
print(type(sj))
